﻿using CloudWorks.Store.BaseClasses;
using System;
using System.Text.Json.Serialization;

namespace CloudWorks.Store.Classes
{
    public class Employee : Person
    {
        public Employee(string name) : this(name, DefaultCheckoutSpeed) { }

        [JsonConstructor]
        public Employee(string name, double checkoutSpeed) : base(name, checkoutSpeed) { }

        public static double DefaultCheckoutSpeed = 0.5;

        public override void Print() => Console.WriteLine($"Employee: {Name}");
    }
}
